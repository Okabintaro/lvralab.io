---
title: Performance
weight: 250
---

# Performance

## Set AMD GPU power profile mode

You can use [CoreCtrl](https://gitlab.com/corectrl/corectrl) to set your power profile mode to VR.

This is important to avoid stuttering.
