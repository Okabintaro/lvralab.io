---
weight: 200
title: WiVRn
---

# WiVRn

- [WiVRn GitHub repository](https://github.com/Meumeu/WiVRn)

> WiVRn lets you run OpenXR applications on a computer and display them on a standalone headset.

It's very similar in purpose to [Monado](/docs/fossvr/monado/), but for standalone VR headsets.
