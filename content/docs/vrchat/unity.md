---
weight: 900
title: Unity
---

# Unity

For the best experience, we found that running Unity from [Flatpak](https://flathub.org/apps/com.unity.UnityHub) is usually preferred.

## Setting up a VRChat avatar/world project

First, take a look at this page from [the official VRChat docs](https://creators.vrchat.com/sdk/upgrade/unity-2022/#using-the-unity-website) about the Unity 2022 upgrade. We'll do something similar.

1. Install Unity Hub from Flatpak.
1. Install the 3rd party VRChat package manager [`vrc-get`](https://github.com/anatawa12/vrc-get).
   - you can install via `cargo install vrc-get`
   - An AUR package is also available here: [`vrc-get` AUR](https://aur.archlinux.org/packages/vrc-get).
1. Open Unity Hub and sign in with your Unity account.
1. Install **2022.3.6f1** (the current supported Unity version, at the time of writing) by running `xdg-open unityhub://2022.3.6f1/b9e6e7e9fa2d`.
1. When prompted, select *Android Build Support* and *Windows Build Support (Mono)*.
1. Create a new project with the **3D Core template**, making sure to name it and give it a better location than `$HOME`.
1. If it opens a window partially offscreen like it did for me (KWin Wayland), press ALT+F3 -> More Actions -> Move.
1. Close the Unity project (will probably crash later if you don't, no big deal but still).
1. Open terminal and `cd` to the project dir.
1. Run either `vrc-get install com.vrchat.avatars` or `vrc-get install com.vrchat.worlds` depending on what you will be making.
1. Reopen the Unity project and it will import the new packages.
1. Go to VRChat SDK -> Show Control Panel and dock it to the main window.

Now you should be able to create and upload avatars and worlds like you normally would.

If you're having issues, consult the [Troubleshooting](#troubleshooting) section.


## Troubleshooting

### AssetBundle was not built

When building and uploading, this error message may appear. It's caused by VRCSDK building an AssetBundle whose filename has mixed caps, when Unity AssetBundles are really only supposed to have lowercase filenames.

The solution is to import this unitypackage: https://github.com/thegu5/VRCSDKonLinux/releases

Importing this will mod the appropriate VRCSDK methods to be case-sensitive-aware.

## Using VCC instead?

If you prefer GUIs, it is now possible to use VRChat Creator Companion (VCC) on Linux.

The official VRChat Creator Companion uses Webview2 (Microsoft Edge). This component is difficult to install, and doesn't run natively. Instead, you can use LinuxCreatorCompanion.

[GitHub repo for LinuxCreatorCompanion](https://github.com/RinLovesYou/LinuxCreatorCompanion)

It works by using wine to extract the necessary bits to re-embed into a native client application.

Build and installation instructions are in the README.

While experimental, you can enjoy much easier avatar, world, and asset creation in this utility, should everything operate as expected. Again, this is experimental, and we discourage you from trying this on important projects. **Remember to make frequent project backups.** Please leave any feedback about bugs or ideas on the GitHub issues tab.
