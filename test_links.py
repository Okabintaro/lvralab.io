#!/usr/bin/env python3
from pathlib import Path
import re
from typing import List
from os.path import exists


def get_all_markdown_files():
    return Path("content").rglob("*.md")


def find_links(f: str) -> List[str]:
    with open(f, "r") as fd:
        content = fd.read()
    return [
        link.lstrip("]").strip("()") for link in re.findall(r"\]\(\/[\w\/]+\)", content)
    ]


def verify_link_exists(link: str) -> bool:
    return exists(f"content{link}/_index.md") or exists(f'content{link.rstrip("/")}.md')


md_files = get_all_markdown_files()

res = 0
for f in md_files:
    for link in find_links(f):
        if not verify_link_exists(link):
            print(f"E: {f}: {link} does not exist")
            res = 1

exit(res)
